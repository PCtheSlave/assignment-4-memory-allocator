#include "mem.h"
#include "mem_internals.h"

static void print_number_test(size_t test_number) { fprintf(stdout, "==========  Тест №%ld RUN ==========\n\n", test_number); }

static void print_error(size_t test_number) { fprintf(stderr, "==========  Тест №%ld FAIL ==========\n\n", test_number); }

static void print_good_test(size_t test_number) { fprintf(stdout, "==========  Тест №%ld GOOD ==========\n\n", test_number); }

static inline struct block_header *find_header(void *contents) {
	return (struct block_header *)((uint8_t *)contents - offsetof(struct block_header, contents));
}

void first_test() {
	print_number_test(1);

	void *heap = heap_init(8192);
	printf("Инициализация кучи:\n");
	debug_heap(stdout, heap);
	printf("\n");

	void *data = _malloc(1000);
	printf("Выделили память:\n");
	debug_heap(stdout, heap);
	printf("\n");
	if (!heap || !data) {
		print_error(1);
		return;
	}

	_free(data);
	printf("Освобождение памяти:\n");
	debug_heap(stdout, heap);
	printf("\n");

	destroy_heap(heap, 8192);
	print_good_test(1);
}

void second_test() {
	print_number_test(2);

	void *heap = heap_init(8192);
	printf("Инициализация кучи:\n");
	debug_heap(stdout, heap);
	printf("\n");

	void *block1 = _malloc(1000);
	void *block2 = _malloc(2000);
	printf("Выделили память:\n");
	debug_heap(stdout, heap);
	if (!heap || !block1 || !block2) {
		print_error(2);
		return;
	}
	printf("\n");

	_free(block1);
	printf("Освобождение одного блока из нескольких:\n");
	debug_heap(stdout, heap);
	printf("\n");

	destroy_heap(heap, 8192);
	print_good_test(2);
}

void third_test() {
	print_number_test(3);
	void *heap = heap_init(8192);

	printf("Инициализация кучи:\n");
	debug_heap(stdout, heap);
	printf("\n");

	void *block1 = _malloc(1000);
	void *block2 = _malloc(2000);
	void *block3 = _malloc(3000);
	printf("Выделили память:\n");
	debug_heap(stdout, heap);
	if (!heap || !block1 || !block2 || !block3) {
		print_error(3);
		return;
	}
	printf("\n");

	_free(block1);
	_free(block2);
	printf("Освобождение двух блоков из нескольких:\n");
	debug_heap(stdout, heap);
	printf("\n");

	destroy_heap(heap, 8192);
	print_good_test(3);
}

void fourth_test() {
	print_number_test(4);
	void *heap = heap_init(8192);

	printf("Инициализация кучи:\n");
	debug_heap(stdout, heap);
	printf("\n");

	void *block1 = _malloc(8192);
	void *block2 = _malloc(8192);
	printf("Выделили память больше чем размер кучи:\n");
	debug_heap(stdout, heap);
	if (!heap || !block1 || !block2) {
		print_error(4);
		return;
	}
	printf("\n");

	_free(block1);
	_free(block2);
	printf("Освобождение блоков:\n");
	debug_heap(stdout, heap);
	printf("\n");

	destroy_heap(heap, 8192);
	print_good_test(4);
}

void fifth_test() {
	print_number_test(5);
	void *heap = heap_init(8192);

	printf("Инициализация кучи:\n");
	debug_heap(stdout, heap);
	printf("\n");

	void *block1 = _malloc(16367);
	printf("Выделили память больше чем размер кучи, блок должен создаться в месте не после кучи,\n поскольку у нас после кучи занято место с преддущего теста:\n");
	debug_heap(stdout, heap);
	if (!heap || !block1) {
		print_error(5);
		return;
	}
	printf("\n");

	_free(block1);
	printf("Освобождение блока:\n");
	debug_heap(stdout, heap);
	printf("\n");

	print_good_test(5);
}
