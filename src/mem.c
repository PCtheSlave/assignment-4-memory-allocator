#define _DEFAULT_SOURCE

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include "mem.h"
#include "mem_internals.h"
#include "util.h"

void debug_block(struct block_header *b, const char *fmt, ...);

void debug(const char *fmt, ...);

extern inline block_size size_from_capacity(block_capacity cap);
extern inline block_capacity capacity_from_size(block_size sz);

static bool block_is_big_enough(size_t query, struct block_header *block) { return block->capacity.bytes >= query; }

static size_t pages_count(size_t mem) { return mem / getpagesize() + ((mem % getpagesize()) > 0); }

static size_t round_pages(size_t mem) { return getpagesize() * pages_count(mem); }

static struct region region_constructor(void *addr, size_t size, bool extends) {
	return (struct region){.addr = addr, .size = size, .extends = extends};
}

static struct block_header block_header_constructor(struct block_header *next, block_capacity capacity, bool is_free) {
	return (struct block_header){.next = next, .capacity = capacity, .is_free = is_free};
}

static block_capacity block_capacity_constructor(size_t bytes) { return (block_capacity){.bytes = bytes}; }

static block_size block_size_constructor(size_t bytes) { return (block_size){.bytes = bytes}; }

static void block_init(void *restrict addr, block_size block_sz, void *restrict next) {
	*((struct block_header *)addr) = block_header_constructor(next, capacity_from_size(block_sz), true);
}

static size_t region_actual_size(size_t query) { return size_max(round_pages(query), REGION_MIN_SIZE); }

extern inline bool region_is_invalid(const struct region *r);

static void *map_pages(void const *addr, size_t length, int additional_flags) {
	return mmap((void *)addr, length, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS | additional_flags, -1, 0);
}

/*  аллоцировать регион памяти и инициализировать его блоком */
static struct region alloc_region(void const *addr, size_t query) {
	query = region_actual_size(size_from_capacity(block_capacity_constructor(query)).bytes);
	void *alloc_reg = map_pages(addr, query, MAP_FIXED_NOREPLACE);
	if (alloc_reg == MAP_FAILED)
		alloc_reg = map_pages(addr, query, 0);
	if (alloc_reg == MAP_FAILED)
		return REGION_INVALID;
	block_init(alloc_reg, block_size_constructor(query), NULL);
	return region_constructor(alloc_reg, query, alloc_reg == addr);
}

static void *block_after(struct block_header const *block);

void *heap_init(size_t initial) {
	const struct region region = alloc_region(HEAP_START, initial);
	if (region_is_invalid(&region))
		return NULL;
	return region.addr;
}

void destroy_heap(void *heap, size_t size) { munmap(heap, size_from_capacity(block_capacity_constructor(size)).bytes); }

#define BLOCK_MIN_CAPACITY 24

static bool block_splittable(struct block_header *restrict block, size_t query) {
	return block && block->is_free && query + offsetof(struct block_header, contents) + BLOCK_MIN_CAPACITY <= block->capacity.bytes;
}

static void *block_after(struct block_header const *block) { return (void *)(block->contents + block->capacity.bytes); }

/*  --- Разделение блоков (если найденный свободный блок слишком большой )--- */
static bool split_if_too_big(struct block_header *block, size_t query) {
	if (block_splittable(block, query)) {
		block_size another_block = block_size_constructor(block->capacity.bytes - query);
		block->capacity.bytes = query;
		void *addr = block_after(block);
		block_init(addr, another_block, block->next);
		block->next = addr;
		block->capacity.bytes = query;
		return true;
	} else
		return false;
}

static bool blocks_continuous(struct block_header const *fst, struct block_header const *snd) {
	return (void *)snd == block_after(fst);
}

static bool mergeable(struct block_header const *restrict fst, struct block_header const *restrict snd) {
	return fst && fst->is_free && snd->is_free && blocks_continuous(fst, snd);
}

/*  --- Слияние соседних свободных блоков --- */
static bool try_merge_with_next(struct block_header *block) {
	if (!block || !block->next || !mergeable(block, block->next))
		return false;
	block->capacity.bytes += size_from_capacity(block->next->capacity).bytes;
	block->next = block->next->next;
	return true;
}

struct block_search_result {
	enum { BSR_FOUND_GOOD_BLOCK, BSR_REACHED_END_NOT_FOUND, BSR_CORRUPTED } type;
	struct block_header *block;
};

/*  --- ... ecли размера кучи хватает --- */
static struct block_search_result find_good_or_last(struct block_header *restrict block, size_t sz) {
	while (block) {
		if (block == block->next)
			return (struct block_search_result){.type = BSR_CORRUPTED, .block = block};
		while (try_merge_with_next(block)) {
		}
		if (block->is_free && block_is_big_enough(sz, block))
			return (struct block_search_result){.type = BSR_FOUND_GOOD_BLOCK, .block = block};
		if (!block->next)
			break;
		block = block->next;
	}
	return (struct block_search_result){.type = BSR_REACHED_END_NOT_FOUND, .block = block};
}

/*  Попробовать выделить память в куче начиная с блока `block` не пытаясь
 расширить кучу Можно переиспользовать как только кучу расширили. */
static struct block_search_result try_memalloc_existing(size_t query, struct block_header *block) {
	struct block_search_result found_block = find_good_or_last(block, query);
	if (found_block.type == BSR_FOUND_GOOD_BLOCK) {
		split_if_too_big(found_block.block, query);
		found_block.block->is_free = false;
		return found_block;
	}
	return found_block;
}

static struct block_header *grow_heap(struct block_header *restrict last, size_t query) {
	if (last) {
		struct region region = alloc_region(block_after(last), query);
		if (!region_is_invalid(&region)) {
			last->next = region.addr;
			if (try_merge_with_next(last))
				return last;
			return last->next;
		}
	}
	return NULL;
}

/*  Реализует основную логику malloc и возвращает заголовок выделенного блока */
static struct block_header *memalloc(size_t query, struct block_header *heap_start) {
	query = size_max(query, BLOCK_MIN_CAPACITY);
	struct block_search_result found_block = try_memalloc_existing(query, heap_start);
	if (found_block.type == BSR_REACHED_END_NOT_FOUND) {
		found_block.block = grow_heap(found_block.block, query);
		found_block = try_memalloc_existing(query, heap_start);
	}
	if (found_block.type == BSR_FOUND_GOOD_BLOCK)
		return found_block.block;
	return NULL;
}

void *_malloc(size_t query) {
	struct block_header *const addr = memalloc(query, (struct block_header *)HEAP_START);
	if (addr)
		return addr->contents;
	else
		return NULL;
}

static struct block_header *block_get_header(void *contents) {
	return (struct block_header *)(((uint8_t *)contents) - offsetof(struct block_header, contents));
}

void _free(void *mem) {
	if (!mem)
		return;
	struct block_header *header = block_get_header(mem);
	header->is_free = true;
	while (try_merge_with_next(header)) {
	}
}
